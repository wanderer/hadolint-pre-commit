# hadolint-pre-commit
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

this repo provides [`pre-commit`](https://pre-commit.com/) hooks for
[`hadolint`](https://github.com/hadolint/hadolint/)


### usage
the following hooks are part of the repo:
* `hadolint` - lints Containerfiles using system `hadolint`
* `hadolint-container` - lints Containerfiles in a container

example usage is as follows:
```yaml
# .pre-commit-config.yaml
---
fail_fast: false
repos:
  - repo: https://git.dotya.ml/wanderer/hadolint-pre-commit
    # rev: v2.10.0
    rev: 9a29f5c23228c332d46379455217f772c7ee6002  # commit or tag or branch
    hooks:
      - id: hadolint
      - id: hadolint-container
...
```

### dependencies
the only dependencies are:
* `pre-commit`, obviously
* `hadolint` for the `hadolint` hook
* `podman`/`docker` installed for the `hadolint-container` hook

### LICENSE
MIT
